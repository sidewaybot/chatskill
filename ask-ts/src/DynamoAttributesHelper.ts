import * as aws from 'aws-sdk';

var doc: aws.DynamoDB.DocumentClient;

export var dynamoAttributesHelper = {
  get: function (table: string, userId: string, callback: (arg1: any, arg2: any) => void) {
    if (!table) {
      callback('DynamoDB Table name is not set.', null);
    }

    if (!doc) {
      doc = new aws.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });
    }

    var params: aws.DynamoDB.DocumentClient.GetItemInput = {
      Key: {
        userId: userId
      },
      TableName: table,
      ConsistentRead: true
    };

    doc.get(params, function (err, data) {
      if (err) {
        console.log('get error: ' + JSON.stringify(err, null, 4));

        if (err.code === 'ResourceNotFoundException') {
          var dynamoClient = new aws.DynamoDB();
          newTableParams['TableName'] = table;
          dynamoClient.createTable(newTableParams, function (err, data) {
            if (err) {
              console.log('Error creating table: ' + JSON.stringify(err, null, 4));
            }
            console.log('Creating table ' + table + ':\n' + JSON.stringify(data));
            callback(err, {});
          });
        } else {
          callback(err, null);
        }
      } else {
        if (isEmptyObject(data) || !data.Item || !('mapAttr' in data.Item)) {
          callback(null, {});
        } else {
          callback(null, data.Item['mapAttr']);
        }
      }
    });
  },

  set: function (table: string, userId: string, data: any, callback: (arg1: any, arg2: any) => void) {
    if (!table) {
      callback('DynamoDB Table name is not set.', null);
    }

    if (!doc) {
      doc = new aws.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });
    }

    var params: aws.DynamoDB.DocumentClient.PutItemInput = {
      Item: {
        userId: userId,
        mapAttr: data
      },
      TableName: table
    };

    doc.put(params, function (err, data) {
      if (err) {
        console.log('Error during DynamoDB put:' + err);
      }
      callback(err, data);
    });
  }
};


function isEmptyObject(obj: any) {
  return !Object.keys(obj).length;
}

var newTableParams: aws.DynamoDB.CreateTableInput = {   // tbd.
  TableName: '',  // ???
  AttributeDefinitions: [
    {
      AttributeName: 'userId',
      AttributeType: 'S'
    }
  ],
  KeySchema: [
    {
      AttributeName: 'userId',
      KeyType: 'HASH'
    }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 5,
    WriteCapacityUnits: 5
  }
};
