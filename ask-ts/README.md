# sidewaybot chatskill-ask-ts library

This is a direct "porting" of [the Alexa Skills Kit SDK for Node.js](https://github.com/alexa/alexa-skills-kit-sdk-for-nodejs) to Typescript.

_Work in progress_


## Overview


To install this library, run:

```bash
$ npm install @sidewaybot/chatskill-ask-ts --save
```

## License

MIT © [Harry Y](mailto:sidewaybot@yahoo.com)
